import Phaser from 'phaser'
import PathFinderPlugin from '../utils/pathfinder'

export default class extends Phaser.Tilemap {

  constructor ({game, key, tileWidth, tileHeight, width, height}) {
    super(game, key, tileWidth, tileHeight, width, height)

    this.addTilesetImage('All', 'mapTiles');
    this.backgroundlayer = this.createLayer('background');
    this.frontLayer = this.createLayer('level');

    this.backgroundlayer.resizeWorld();
    this.frontLayer.resizeWorld();

    let walkableTiles = [3, 168, 167, 153, 137, 151, 69, 19, 52, 168, 18]
    this.pathfinder = this.game.plugins.add(PathFinderPlugin);
    this.pathfinder.setGrid(this.layers[1].data, walkableTiles);

    this.debugPath = this.game.add.group();
  }

  update () {}

  findPathTo(fromX, fromY, toX, toY, callbackFunction) {
    if(!callbackFunction) return;

    this.debugPath.removeChildren();

    this.pathfinder.setCallbackFunction(callbackFunction);
    this.pathfinder.enableDiagonals();
    this.pathfinder.preparePathCalculation([fromX, fromY], [toX, toY]);
    this.pathfinder.calculatePath();
  }

}
