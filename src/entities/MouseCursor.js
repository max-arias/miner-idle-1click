import Phaser from 'phaser'

export default class extends Phaser.Sprite {

  constructor ({ game, x, y, key, frame}) {
    super(game, x, y, key, frame)
  }

  update () {
    this.x = this.game.input.mousePointer.x;
    this.y = this.game.input.mousePointer.y;
  }

}
