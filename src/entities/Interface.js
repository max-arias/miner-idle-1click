class Interface {
  constructor(game, slickUI) {
    this.panel = null;
    this.button = null;
    this.game = game;
    this.slickUI = this.game.plugins.add(Phaser.Plugin.SlickUI);
    this.slickUI.load('assets/kenney-theme/kenney.json'); 
  }

  createInterface() {

    this.minedGold = this.game.add.bitmapText(10, 10, 'carrier_command', '0', 12);

    this.menuButton = new window.SlickUI.Element.DisplayObject(this.game.width - 45, 10, this.game.make.sprite(0, 0, 'menu-button'));
    this.slickUI.add(this.menuButton);
    this.menuButton.inputEnabled = true;
    this.menuButton.input.useHandCursor = true;

    this.panel = new window.SlickUI.Element.Panel(this.game.width - 155, 10, 150, this.game.height - 16);
    this.slickUI.add(this.panel);
    this.panel.visible = false;
    
    this.button = new window.SlickUI.Element.Button(0,0, 140, 80)
    this.panel.add(this.button);
    this.button.add(new window.SlickUI.Element.Text(0,0, "My button")).center();

    this.panel.add(this.closePanelButton = new window.SlickUI.Element.Button(0, this.game.height - 76, 140, 40));
    this.closePanelButton.add(new window.SlickUI.Element.Text(0,0, "Close")).center();
    this.closePanelButton.inputEnabled = true;

    this.closePanelButton.events.onInputUp.add((pointer, event, blah) => {
      this.game.clickedMenu = true;
      this.panel.visible = false;
      return false;
    });

    this.menuButton.events.onInputUp.add((e) => {
      this.panel.visible = true;
    });
    
  }
}

export { Interface as default}