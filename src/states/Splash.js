import Phaser from 'phaser'
import { centerGameObjects } from '../utils'

export default class extends Phaser.State {
  init () {}

  preload () {
    this.loaderBg = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loaderBg')
    this.loaderBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loaderBar')
    centerGameObjects([this.loaderBg, this.loaderBar])

    this.load.setPreloadSprite(this.loaderBar)
    //
    // load your assets
    //
    game.load.tilemap('map1', 'assets/tilemap/map1.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.spritesheet('playerTiles', 'assets/images/32x32/Animated.png', 32, 32, 10);
    game.load.spritesheet('mouseSheet', 'assets/images/mouse-sprite.png', 16, 16);

    game.load.image('mapTiles', 'assets/images/32x32/All.png');
    game.load.image('mouseCursor', 'assets/images/mouse.png');
    game.load.image('debugDot', 'assets/images/dot.png');

    game.load.image('menu-button', 'assets/images/menu.png');

    game.load.bitmapFont('carrier_command', 'assets/fonts/carrier_command.png', 'assets/fonts/carrier_command.xml');
  }

  create () {
    this.state.start('Game')
  }
}
