/* globals __DEV__ */
import Phaser from 'phaser'
import PathFinderPlugin from '../utils/pathfinder'
import Map from '../entities/Map'
import Player from '../entities/Player'
import MouseCursor from '../entities/MouseCursor'
import Interface from '../entities/Interface'

export default class extends Phaser.State {
  init () {
    this.map = null;
  }

  preload () {
    this.interface = new Interface(this.game);
  }

  create () {

    this.game.physics.startSystem(Phaser.Physics.ARCADE);    

    this.map = new Map({
      game: this.game,
      key: 'map1'
    });

    this.player = new Player({
      game: this.game,
      x: 160,
      y: 480,
      key: 'playerTiles',
      frame: 1
    });

    this.workerGroup = this.game.add.group()
    this.workerGroup.add(this.player);

    // this.mouseCursor = new MouseCursor({
    //   game: this.game,
    //   x: 0,
    //   y: 0,
    //   key: 'mouseSheet',
    //   frame: 0
    // })
    // this.game.add.existing(this.mouseCursor);

    this.game.input.onDown.add(this.clickListener, this);    
    this.interface.createInterface();
  }

  render () {}

  clickListener (pointer, event) {
    if(this.player.tween && this.player.tween.isRunning) {
      this.player.tween.stop();
    }

    let playerPos = {
      x: Math.floor(this.player.x / 32),
      y: Math.floor(this.player.y / 32)
    }

    let toPos = {
      x: Math.floor(this.game.input.activePointer.x / 32),
      y: Math.floor(this.game.input.activePointer.y / 32)
    };

    this.map.findPathTo(playerPos.x, playerPos.y, toPos.x, toPos.y, (path) => {
      path = path || [];
      
      if(!path.length) return;

      let tweenChain = {x: [], y: []}
      for(var i = 0, ilen = path.length; i < ilen; i++) {
        tweenChain.x.push(path[i].x * 32);
        tweenChain.y.push(path[i].y * 32);
        this.map.debugPath.create(path[i].x * 32, path[i].y * 32,  'debugDot');
      }

      this.player.tween = this.game.add.tween(this.player);
      this.player.tween.to({ x: tweenChain.x, y: tweenChain.y}, (150 * ilen), "Linear");
      this.player.tween.start();
    });

  }
}
